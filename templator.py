from tree import E
from translator import PbProxy, pbIter, PbError, toValue, pbValue


class PbTemplatorError(PbError):
    pass


class R:
    def create(s):
        e = E()
        e.t = 'string'
        e.v = s
        f = E()
        f.t = 'const'
        f.v = '?'
        return E.func('`apply', [f, e])

    def iff(e):
        return (
            E.isFunc(e, '`apply')
            and e[0].t == 'const'
            and e[0].v == '?'
            and len(e) >= 2
        )

    def v(e):
        if not R.iff(e):
            return (0, 0)
        if (
            E.isFunc(e[1], '…')
            and len(e[1]) == 2
            and E.isErr(e[1][0])
        ):
            if E.isFunc(e[1][1], '+'):
                return (3, (e[2], [toValue(v) for v in e[1][1].args]))
            if len(e) == 3:
                return (3, (e[2], [toValue(e[1][1])]))
            return (2, toValue(e[1][1]))
        return (1, toValue(e[1]))

    def v1(e):
        t, v = R.v(e)
        return v if t == 1 else None

    def vm(e):
        t, v = R.v(e)
        return v if t == 2 else None

    def va(e):
        t, v = R.v(e)
        return v if t == 3 else None

    def put(data, v, eqn):
        if v in data:
            raise PbTemplatorError(
                'Eject error: multiple template\n%s [%s] = %s' % (
                    data, repr(v), eqn
                )
            )
        data[v] = eqn

    def get(data, v):
        r = data(v)
        if r is None:
            raise PbTemplatorError(
                'Inject error: no template\n%s [%s]' % (
                    data, repr(v)
                )
            )
        return r


def eject(eqn, src, data):
    v = R.v1(src)
    if v is not None:
        R.put(data, v, eqn)
        return True
    if type(eqn) != type(src):
        return False
    if isinstance(eqn, E):
        if eqn.t != src.t:
            return False
        if eqn.t != 'f':
            return eqn == src
        if (
            eqn.v != src.v
            or len(eqn) < len(src)
        ):
            return False
        if eqn.isFunc('≺'):
            return eqn == src
    elif type(eqn) in [list, tuple]:
        pass
    else:
        return eqn == src
    for i in range(len(eqn)):
        v = R.vm(src[i])
        if v is not None:
            R.put(data, v, eqn[i:])
            return True
        if not eject(eqn[i], src[i], data):
            return False
    return True


def _proxy_data(data, v, i):
    def res(k):
        if k in v:
            r = data(k)
            if r is not None:
                r = r(i)
            if r is None:
                raise PbTemplatorError(
                    'Inject error: no template\n%s [%s] [%s]' % (
                        data, repr(k), repr(i)
                    )
                )
            return r
        return R.get(data, k)
    return res


def inject(dst, data):
    v = R.v1(dst)
    if v is not None:
        # TODO correct?
        # E.func('asd', [?(0)])
        # ?(0) == 1 -- work
        # ?(0) == [1, 2, 3] -- inpossible
        # ?(0) == PbSeries -- non't work
        # ?(0) == PbProxy is variant of PbSeries, but work
        return pbValue(R.get(data, v))
    if isinstance(dst, E):
        if dst.t != 'f':
            return dst
    elif type(dst) in [list, tuple]:
        pass
    else:
        return dst
    args = []
    for e in dst:
        t, v = R.v(e)
        if t == 2:
            args += pbIter(R.get(data, v))
            break
        if t == 3:
            t, v = v
            for i, _ in enumerate(pbIter(R.get(data, v[0]))):
                args.append(inject(
                    t,
                    _proxy_data(data, v, i)
                ))
            break
        args.append(inject(e, data))
    if isinstance(dst, list):
        return args
    if isinstance(dst, tuple):
        return tuple(args)
    assert isinstance(dst, E)
    return E.func(dst.v, args)


def _apply(eqn, src, dst, isRec):
    data = {}
    r = eject(eqn, src, data)
    if r:
        if isRec:
            for k in data:
                data[k] = _apply(data[k], src, dst, isRec)
        d = dst(data)
        if d is None:
            if not isRec:
                return eqn
        else:
            return d
    if isRec is None:
        return eqn
    if type(eqn) in [list, tuple]:
        elargs = eqn
    elif isinstance(eqn, E):
        if eqn.t != 'f':
            return eqn
        if eqn.isFunc('≺'):
            return eqn
        elargs = eqn.args
    else:
        return eqn
    args = []
    ident = True
    for e in elargs:
        r = _apply(e, src, dst, isRec)
        if e is not r:
            ident = False
        args.append(r)
    if ident:
        return eqn
    else:
        if isinstance(eqn, list):
            return args
        if isinstance(eqn, tuple):
            return tuple(args)
        assert isinstance(eqn, E)
        return E.func(eqn.v, args)


def _proxy_list(v):
    if isinstance(v, list):
        return PbProxy.call(v, PbProxy.CALLS.kv)
    else:
        return v


def apply(eqn, src, dst, isRec=True):
    """
    "..." is syntax tree as latex string
    '...' is simple string

    let src = "?(\text{a}) + ?(\text{b})"

    if dst = "?(\text{a}) - ?(\text{b})"
        let dstf = lambda data: f"{data['a']} - f{data['b']}"

    if dst = lambda data: <custom syntax tree builder>
        let dstf = dst

    let isRec = True

    let eqn = "(a + b) + c"

    THEN workflow is:
        "(a + b) + c"
        data = {'a': "a + b", 'b': "c"}
        from data:
            "a + b"
            data = {'a': "a", 'b': "b"}
            from data:
                "a"
                -> "a"
                "b"
                -> "b"
            dstf(data) == None
            from '+' args:
                "a"
                -> "a"
                "b"
                -> "b"
            -> "a + b"
            "c"
            -> "c"
        dstf(data) == "(a + b) - c"
        -> "(a + b) - c"

    let isRec = False

    let eqn = "((a + b) + c) * (d + e)"

    THEN workflow is:
        "((a + b) + c) * (d + e)"
            from '*' args:
            "(a + b) + c"
            data = {'a': "a + b", 'b': "c"}
            dstf(data) == None
            -> "(a + b) + c"
            "d + e"
            data = {'a': "d", 'b': "e"}
            dstf(data) == ("?(a) - ?(b)", {'a': "2", 'b': "3"})
            -> "2 - 3"
        -> "((a + b) + c) * (2 - 3)"
    """
    if R.v1(src) is not None:
        isRec = False
    if isinstance(dst, E):
        ldst = dst
        def dst(data): return data('?', ldst)
    if not isinstance(dst, type(lambda: 0)):
        raise PbTemplatorError(
            'Thats dst not a tree or function:\n%s' % (
                str(dst)
            )
        )

    def dstf(data):
        data = (
            PbProxy
            .call(data, PbProxy.CALLS.kv)
            .ret(_proxy_list)
        )
        try:
            res = dst(data)
        except Exception:
            raise PbTemplatorError(
                'During call dst func with:\n%s' % str(data)
            )
        if res is None:
            return None
        if isinstance(res, E):
            return res
        try:
            (ef, e) = (res('!'), res('?'))
        except Exception:
            raise PbTemplatorError(
                'During call:\n%s' % str(res)
            )
        if ef is not None:
            return ef
        e = res('?')
        if not isinstance(e, E):
            raise PbTemplatorError(
                'Thats dst result not a tree:\n%s' % (
                    str(e)
                )
            )
        return inject(e, res)
    eqn = _apply(eqn, src, dstf, isRec)
    return E.squeeze(eqn)
