import os

from tree import create
from translator import PbCodeHelper, cody, execute, Unclosure
from consty import consty
from logs import log, INFO
# from view import latex2png


def fastExec(s):
    s = create(s)
    s = consty(s)
    s = Unclosure.bind(s)
    # log(INFO, s)
    s = cody(s, 'py', 'generators/py.py')
    log(INFO, '-' * 20)
    log(INFO, s)
    s = execute(s)
    s = s()
    log(INFO, '-' * 20)
    log(INFO, s)
    return s


def get_test(fn):
    with open(os.path.join('pbtests', fn), 'r', encoding='utf8') as f:
        return f.read()


def pb_py_test(cfn=None):
    [(_, _, fns)] = [i for i in os.walk('pbtests')]
    ignored = ['consty_', 'rules_', 'json.pb']
    for fn in fns:
        if cfn is not None and cfn != fn:
            continue
        if len([0 for fni in ignored if fn[:len(fni)] == fni]) > 0:
            continue
        s = get_test(fn)
        print('Test file <%s>' % fn)
        try:
            fastExec(s)
        except Exception:
            raise Exception('Error with file <%s>' % fn)


def test_all():
    pb_py_test()
    test_templator()
    test_code_injects()
    test_filer()
    test_js_oneline_func()
    test_consty_classic()
    # TODO it noesn't work with consty
    # Cuz if we can run someth
    # We squeeze it
    # test_analyze_classic()
    # test_analyze_series()
    test_typalize_rec()
    test_pypb()
    test_consty_gc()
    test_consty_rules()
    test_msub_wrap()
    test_rules_parser()
    test_left_right()
    test_consty_series()
    print('>>> ALL TESTS PASSED <<<')


def test_templator():
    from templator import apply, PbTemplatorError
    # Classic
    src = create(r"?(\text{a}) + ?(3)")[0]
    dst = create(r"?(\text{a}) * 2 - ?(3)")[0]
    eqn1pre = create(r"""
    f(x) = x * (3 + \text{hello world}) - (6+((3+2)*x)^2);
    f(10)
    """)
    # Rec & norec
    eqn2rec = create(r"""
    f(x) = x * (3 * 2 - \text{hello world}) - (6 * 2 - ((3*2-2)*x)^2);
    f(10)
    """)
    eqn2norec = create(r"""
    f(x) = x * (3 * 2 - \text{hello world}) - (6 * 2 - ((3+2)*x)^2);
    f(10)
    """)
    eqn1rec = apply(eqn1pre, src, dst)
    eqn1norec = apply(eqn1pre, src, dst, False)
    assert eqn1rec == eqn2rec
    assert eqn1norec == eqn2norec
    # Test latex gen code (TODO: why?)
    [eqn1rec, eqn1norec, eqn2rec, eqn2norec] = [
        cody(s, 'py', 'generators/latex.py') for s in [
            eqn1rec, eqn1norec, eqn2rec, eqn2norec
        ]
    ]
    assert eqn1rec == eqn2rec
    assert eqn1norec == eqn2norec
    # Test not found
    eqn1nofind = create("2 * 3")
    assert eqn1nofind is apply(eqn1nofind, src, dst)
    # Test args
    src = create(r"\prec(f(?(0), 2, ?(...1)) + ?(...\text{asd}) * 0)")[0][2]
    dst = create(r"""
        \prec(
            (?(...1) + 0)
            * ?(0)
            / g(?(\text{emp}), 3, ?(...1 + \text{asd}, ?(1) ^ ?(\text{asd})))
        )
    """)[0][2]
    eqn = create(r"\prec(f(1,2,3,4,5,q,w) + 6*7*8*9*0)")[0][2]

    def dstf(data):
        data('emp', create(r"\prec(45)")[0][2])
        data('?', dst)
        return data
    res = apply(eqn, src, dstf)
    exp = create(r"\prec((3+4+5+q+w)*1/g(45,3,3^6,4^7,5^8,q^9,w^0))")[0][2]
    assert res == exp
    # Test recursive args
    src = create(r"\prec(?(...0) + 0)")[0][2]
    dst = create(r"\prec(?(...0) * 0)")[0][2]
    eqn = create(r"\prec(2 + f(1 + 2 + 3) + 4)")[0][2]
    eqn1 = apply(eqn, src, dst)
    eqn2 = apply(eqn, src, dst, False)
    assert eqn1 == create(r"\prec(2 * f(1 * 2 * 3) * 4)")[0][2]
    assert eqn2 == create(r"\prec(2 * f(1 + 2 + 3) * 4)")[0][2]
    # Test eject & inject args in list
    src = create(r"\prec(\left\{ ?(...0) \right\})")[0][2]
    dst = create(r"\prec(?(...0) * 0)")[0][2]
    eqnArr = create(r"\prec(\left\{ 1, 2, 3, 4 \right\})")[0][2]
    eqnArgs = create(r"\prec(1 * 2 * 3 * 4)")[0][2]
    res = apply(eqnArr, src, dst)
    assert res == eqnArgs
    res = apply(eqnArgs, dst, src)
    assert res == eqnArr
    # Test inject map of args
    src = create(r"\prec(?(...0) * 0 + ?(...1) * 1)")[0][2]
    dst = create(r"\prec(\left\{ 0, ?(...0+1, ?(0) ^ ?(1)) \right\})")[0][2]
    eqn = create(r"\prec(1*2*3 + 4*5*6)")[0][2]
    exp = create(r"\prec(\left\{ 0, 1^4, 2^5, 3^6 \right\})")[0][2]
    res = apply(eqn, src, dst)
    assert res == exp
    # Test injext map of args with diff len
    eqn = create(r"\prec(1*2*3 + 4*5)")[0][2]
    try:
        apply(eqn, src, dst)
        assert False
    except PbTemplatorError:
        pass


def test_code_injects():
    s = r"""
    \sin = \inject(\text{__import__('math').sin});
    v = \inject(\text{3}) + 1;
    \begin{cases}
        n_i = i
    \end{cases};
    n_3 = m_3;
    m_5 = \sin(10);
    m_3 + m_5 + v
    """
    s = fastExec(s)
    assert s == __import__('math').sin(10) + 3 + 3 + 1


def test_filer():
    import shutil
    from filer import walk
    fld = 'filer_tests'
    os.mkdir(fld)
    try:
        s = '1+2+3\n'
        fs = ['f.py.pb', 'f.js.pb']
        for f in fs:
            with open(os.path.sep.join((fld, f)), 'w', encoding='utf8') as f:
                f.write(s)
        walk(fld)
        for f in fs:
            f = '.'.join(f.split('.')[:-1])
            assert os.path.isfile(os.path.sep.join((fld, f)))
        err = None
    except Exception as e:
        err = e
    shutil.rmtree(fld)
    if err is not None:
        raise err


def test_js_oneline_func():
    s = get_test('js_oneline_func.pb')
    s = create(s)
    s = cody(s, 'py', 'generators/js.py')
    log(INFO, s)
    assert s.find('(x,y,z){') >= 0


def test_consty_classic():
    s = get_test('consty_classic.pb')
    s = create(s)
    s = consty(s)
    log(INFO, '-' * 10, 'CONSTY', '-' * 10)
    log(INFO, s)
    s = Unclosure.bind(s)
    s = cody(s, 'py', 'generators/py.py')
    s = execute(s)
    s = s()
    log(INFO, s)
    assert [s(i) for i in range(4)] == [6, 6, 11, 46]
    assert s(4)(4) == 7


def test_consty_gc():
    s = get_test('consty_gc.pb')
    s = create(s)[0]
    for s in s[2:]:
        log(INFO, '-' * 10, 'BEFORE CONSTY', '-' * 10)
        log(INFO, s)
        s = consty(s)
        log(INFO, '-' * 10, 'CONSTY', '-' * 10)
        log(INFO, s)
        ss = s
        assert s.isFunc(';')
        assert s[0].isFunc('`repl')
        if s[0][0].t == 'fake':
            s = s[-1]
            assert s.isFunc(';')
            assert s[0].isFunc('`repl')
        assert len(s.args) == 2
        assert s[0][0].v == 'f'
        assert s[1].isFunc('`apply')
        assert s[1][0].v == 'f'
        s = ss
        s = Unclosure.bind(s)
        s = cody(s, 'py', 'generators/py.py')
        s = execute(s)
        s = s()
        assert s(2) == 5


def test_consty_rules():
    s = get_test('consty_rules.pb')
    s = create(s)[0]
    for s in s[2:]:
        log(INFO, '-' * 10, 'BEFORE CONSTY', '-' * 10)
        log(INFO, s)
        s = consty(s)
        log(INFO, '-' * 10, 'CONSTY', '-' * 10)
        log(INFO, s)
        s = Unclosure.bind(s)
        s = cd = cody(s, 'py', 'generators/py.py')
        s = execute(s)
        s = s()
        log(INFO, 'Result =>', s)
        try:
            assert s(0)(3, 3) == 6
            assert s(1)(3, 3) == 0
            assert s(2)(3, 3) == 9
            assert s(3)(3, 3) == 1
            assert s(4) is None
        except Exception:
            raise PbCodeHelper(cd)


def test_analyze_classic():
    from analyze import walk as anal_walk
    s = get_test('types_classic.pb')
    s = create(s)
    s = consty(s)
    anal_walk(s, False)
    # all
    assert s.AE[0].strtype() == 'int'
    # f(a)
    assert s[0].AE[0].strtype() == 'function<int(int)>'
    # a
    assert s[1].AE[0].strtype() == 'float'
    # b
    assert s[2].AE[0].strtype() == 'int'
    # c
    assert s[3].AE[0].strtype() == 'float'


def test_analyze_series():
    from analyze import walk as anal_walk
    s = get_test('types_series.pb')
    s = create(s)
    s = consty(s)
    anal_walk(s, False)
    assert s.AE[0].strtype() == 'int'
    # f
    assert s[0].AE[0].strtype() == 'function<int(int)>'
    # g
    assert s[1].AE[0].strtype() == 'function<int(int)>'
    # h
    assert s[2].AE[0].strtype() == 'function<int(int)>'
    # q
    assert s[5].AE[0].strtype() == 'function<int(int,int)>'
    # r
    assert s[6].AE[0].strtype() == 'function<int(int,int)>'
    # w
    assert s[7].AE[0].strtype() == 'function<float(int,int)>'


def test_typalize_rec():
    from typalize import Types
    s = get_test('types_rec.pb')
    s = create(s)
    Types.walk(s)


def test_pypb():
    import pypb
    s = ("""
def f(x):
    def w(x, y):
        x + y
    def g(q): w(x, q)
    g
a = f(3)
assert a(4) == 7
""")
    s = pypb.convert(s)
    log(INFO, '-' * 10, 'PYPB RESULT', '-' * 10)
    log(INFO, s)
    log(INFO, '-' * 10)
    s = Unclosure.bind(s)
    s = cody(s, 'py', 'generators/py.py')
    log(INFO, s)
    s = execute(s)
    s = s()
    log(INFO, 'returns', s)


def test_msub_wrap():
    res = create(r"""\prec({
        e_1 = 1;
        e_3 = 1;
        \begin{cases}
            e_1, 1 \\
            e_1(3), else
        \end{cases}
    })""")[0][2]
    res = repr(res)
    exp = (
        "<;(`repl(msub([e], 1), [], 1), "
        "`repl(msub([e], 3), [], 1), "
        "`cases(,(`apply(msub([e], 1)), 1), "
        "`apply(msub([e], 1), 3)))>"
    )
    log(INFO, exp)
    log(INFO, res)
    assert exp == res


def test_rules_parser():
    s = get_test('rules_parser.pb')
    s = create(s)
    s = consty(s)
    log(INFO, '-' * 10, 'CONSTY', '-' * 10)
    log(INFO, s)
    assert s.isFunc('`series')
    assert s[0] == 'arrN'
    s = Unclosure.bind(s)
    s = cody(s, 'py', 'generators/py.py')
    s = execute(s)
    s = s()
    assert s(0)(0) is s(2)(0)
    assert s(0)(1) is s(1)(1)
    assert s(1)(0) == s(3)
    assert s(2)(1) == s(4)
    assert s(5).isFunc('+')
    assert s(6).isFunc(',')


def test_left_right():
    # Correct [] handling
    s = r"\prec( []\left[][\right\] )"
    s = create(s)
    s = s[0][2]
    assert s == create(
        r"\prec(\left[ \right ] \left\[ \right] \left[ \right])"
    )[0][2]
    # Ignore spaces
    s = r"\begin    {cases} f_x = x \\ f_0 = 0 \end    {cases}; f"
    s = create(s)
    assert s == create(
        r"\begin{cases} f_x = x \\ f_0 = 0 \end{cases}; f"
    )
    # Test () equivalent
    s = [
        # \left\(
        r"\left\( \begin{cases} f_x = x \\"
        r" f_0 = \left\( 0 \right\) \end{cases} \right\); f",
        # \left(
        r"\left( \begin{cases} f_x = x \\"
        r" f_0 = \left( 0 \right) \end{cases} \right); f",
        # (
        r"( \begin{cases} f_x = x \\ f_0 = ( 0 ) \end{cases} ); f",
        # {
        r"{ \begin{cases} f_x = x \\ f_0 = { 0 } \end{cases} ; f}"
    ]
    s = [create(si) for si in s]
    s = [si == s[0] for si in s[1:]]
    assert min(s)
    # Test [] names
    s = r"s = 3; [asdErt](y) = 5; [d]([xyz]) = 6; s + [asdErt] + [d]"
    s = create(s)
    assert s.isFunc(';')
    assert len(s) == 4
    s = s[:-1]
    assert min(
        [si.isFunc('`repl') and si[0].t == 'const' for si in s]
    )
    s = [si[0].v for si in s]
    assert s == ['s', 'asdErt', 'd']


def test_consty_series():
    s = get_test('consty_series.pb')
    s = create(s)
    s = consty(s)
    log(INFO, '-' * 10, 'CONSTY', '-' * 10)
    log(INFO, s)
    s = Unclosure.bind(s)
    s = cody(s, 'py', 'generators/py.py')
    s = execute(s)
    s = s()
    parser = s(0)
    def eqns(i): return s(1)(i+2)

    def test(eqn, tp, ln):
        if tp == 'mapN':
            assert eqn[0][2][1].t == 'string'
            assert eqn[0][2][1].v == 'operators/args'
            eqn = eqn[3][2]
            assert len(eqn) == ln + 1
            eqn = eqn[-1]
            assert len(eqn) == ln + 1
            return eqn[-1]
        if tp == 'flatmapN':
            assert eqn.isFunc('`pipely')
            assert eqn[0][1].t == 'string'
            assert eqn[0][1].v == 'operators/flat-map'
            return test(eqn[1], 'mapN', ln)
        if tp == 'genN':
            eqn = test(eqn, 'flatmapN', ln)
            assert eqn.isFunc('`cases')
            return eqn[0][1]
        assert False
    import time
    eqn = create(r"\prec(i < 0)")[0][2]
    t = time.time()
    for i in range(33):
        test(parser(eqns(0)), 'genN', 1)
        test(parser(eqns(1)), 'mapN', 1)
        test(parser(eqns(2)), 'mapN', 3)
        assert parser(eqns(3)).isErr()
        assert parser(eqns(4)).isErr()
        assert parser(eqns(5)).isErr()
        test(parser(eqns(6)), 'flatmapN', 2)
        res = test(parser(eqns(7)), 'mapN', 3)
        assert test(res, 'genN', 1) == eqn
    t = time.time() - t
    print('consty series test ran at %f sec' % t)


# test_typalize_rec()
# test_templator()
# test_consty_series()
# test_rules_parser()
