from tree import E, create
from translator import cody, execute, Unclosure, getPbType
from templator import apply
from logs import log, WARN, INFO

CONST = 1
NOCONST = 0

seriessy = None


class Incair:
    def __init__(self):
        self.txt = None
        self.usages = None
        self.fastWalk = None
        self.fakeName = None
        self.origin = None

    def getRepl(self):
        return E.func('`repl', [
            self.fakeName,
            [],
            self.origin
        ])

    def getApply(self):
        return E.func('`apply', [
            self.fakeName
        ])

    def __repr__(self):
        return (
            f"<{repr(self.txt)}: {repr(self.usages)} as {repr(self.fakeName)}>"
        )

    def createIncludes():
        incair = Incair()
        incair.faker = E.Fake('include')
        return [incair]

    def withIncludes(includes, el, validate=False):
        incairs = [
            incair.getRepl()
            for incair in includes
            if incair.origin is not None
        ]
        if validate:
            assert len(incairs) == len(includes)
        return E.func(';', incairs + [el])


def _isConst(el):
    if el.t in ['string', 'number', 'zero']:
        return True
    if el.v == '≺':
        return True
    return False


def _include(el, includes):
    if el.t != 'string':
        raise Exception('Unsupported include\n%s' % el)
    incair = None
    for i in includes:
        if i.txt == el:
            incair = i
    if incair is not None:
        incair.usages += 1
        if incair.fastWalk is None:
            raise Exception('Circular include\n%s' % incair)
        return incair.fastWalk
    incair = Incair()
    includes.append(incair)
    incair.txt = el
    incair.usages = 1
    incair.fakeName = includes[0].faker.fake()
    fn = el.v.split('/')
    if fn[0] != '':
        fn = ['pbstl'] + fn
    fn[-1] += '.pb'
    import os
    fn = os.path.sep.join(fn)
    try:
        with open(fn, 'r', encoding='utf8') as f:
            s = f.read()
    except Exception:
        raise Exception('While include %s' % str(el))
    log(INFO, 'Include =>', fn)
    s = create(s)
    with MACRO.CTX(seriessy) as ex:
        s = ex(s)
    (c, s, res) = _walk(s, [], includes)
    log(INFO, '-' * 50)
    incair.fastWalk = (c, s, incair.getApply())
    incair.origin = res
    curi = [i for i, e in enumerate(includes) if e is incair][0]
    while True:
        if curi == 1:
            break
        if includes[curi-1].origin is not None:
            break
        includes[curi] = includes[curi-1]
        curi -= 1
    includes[curi] = incair
    return incair.fastWalk


def _res2E(res):
    if res is None:
        el = E()
        el.t = 'const'
        el.v = '⊗'
        return E.func('`apply', [el])
    if type(res) in [int, float]:
        el = E()
        el.t = 'number'
        el.v = str(res)
        return el
    if type(res) in [str]:
        el = E()
        el.t = 'string'
        el.v = res
        return el
    if type(res) in [bool]:
        el = E()
        el.t = 'const'
        el.v = '⊤' if res else '⊥'
        return E.func('`apply', [el])
    if type(res) == E:
        if res.isFunc('≺'):
            return res
        return E.func('≺', [res])
    if isinstance(res, type(lambda: 0)):
        return None
    if isinstance(res, type(print)):
        return None
    if isinstance(res, E.Fake):
        return None
    if getPbType(res) is not None:
        return None
    log(WARN, 'Warn => unknown type of expression result => %s' % str(res))
    return None


def _gorun(el, repls, includes):
    s = Incair.withIncludes(includes, el)
    s.args = s.args[:-1] + repls + s.args[-1:]
    (_, s) = _gc(s, [], False)
    s = Unclosure.bind(s)
    MACRO.clear()
    s = cody(s, 'py', 'generators/py.py')
    s = execute(s)
    s = s()
    s = _res2E(s)
    return s


UNCERT_SRC = create(r"""
    \prec({
        ?(0) + ?(1) + ?(2) + ?(3)
    })
""")[0][2]


UNCERT_DST = create(r"""
\prec(
    \begin{cases}
        ?(0), ?(1) \equiv ?(2) \\
        ?(3), else
    \end{cases}
)
""")[0][2]


def _insert_uncert(el, uncert):
    eqn = E.func('≻', [
        uncert.args[0],
        E.func('+', [
            uncert.args[1][0],
            el[1],
            E.func('≺', [uncert.args[1][1]]),
            el
        ]),
        uncert.args[2]
    ])
    res = apply(eqn, UNCERT_SRC, UNCERT_DST, False)
    return res


def _insert(el, uncert):
    if type(el) in [list, tuple]:
        if len(el) == 0:
            return (False, el)
        res = [_insert(e, uncert) for e in el]
        if not max(e[0] for e in res):
            return (False, el)
        res = [e[1] for e in res]
        if type(el) == tuple:
            res = tuple(res)
        return (True, res)
    if not isinstance(el, E):
        return (False, el)
    if el.t != 'f':
        return (False, el)
    if el.v == '≻' and el[0] == uncert.obj[0]:
        return (True, _insert_uncert(el, uncert))
    if len(el.args) == 0:
        return (False, el)
    args = [_insert(e, uncert) for e in el.args]
    if not max(e[0] for e in args):
        return (False, el)
    nel = E.func(el.v, [e[1] for e in args])
    return (True, nel)


def _insert_full(el, repls, uncert, includes):
    if len(uncert.args) != 1 or not isinstance(uncert.args[0], E):
        try:
            raise uncert
        except Exception:
            raise Exception('Unsupported')
    (uncertEl,) = uncert.args
    with MACRO.CTX(seriessy) as ex:
        uncertRes = ex(uncertEl)
    (uncertC, uncertS, uncertRes) = _walk(uncertRes, [], includes)
    uncert.args = [0, (uncertRes, uncertEl), (uncertC, uncertS)]
    (isChange, nel) = _insert(el, uncert)
    if isChange:
        return nel
    ri = None
    for ii in range(len(repls)):
        (isChange, nel) = _insert(repls[ii], uncert)
        if not isChange:
            continue
        if ri is not None:
            raise Exception('Multi nodes find by uncert in repls')
        ri = ii
        break
    if ri is not None:
        return E.func(';', [nel] + repls[ri+1:] + [el])
    ri = None
    for incair in includes:
        if incair.origin is None:
            continue
        (isChange, nel) = _insert(incair.getRepl(), uncert)
        if not isChange:
            continue
        if ri is not None:
            raise Exception('Multi nodes find by uncert in includes')
        ri = incair
        break
    if ri is not None:
        log(INFO, 'Replaces:', incair)
        return E.func(';', [nel] + repls + [el])
    raise Exception('Cannot find uncert node')


def _run(pr, origRepls, includes):
    if pr[0] != CONST or _isConst(pr[1]):
        return pr
    el = pr[1]
    repls = [
        repl
        for _, s, repl in origRepls
        if len(s) == 0
    ]
    iter = 0
    while True:
        iter += 1
        log(INFO, 'Iter =>', iter)
        try:
            res = _gorun(el, repls, includes)
        except Exception as e:
            res = MACRO.find_throwed_uncert(e)
            if res is None:
                raise Exception('During run\n%s' % str(el))
        if isinstance(res, MACRO.Uncert):
            log(INFO, 'Uncert =>', res)
            el = _insert_full(el, repls, res, includes)
            el = _walk(el, origRepls, includes)
            if len(el[1]) > 0:
                raise Exception('Unexpected vars\n%s' % str(el[1]))
            el = el[2]
            continue
        log(INFO, 'Res =>', res)
        if res is None:
            return (NOCONST, el)
        else:
            return (CONST, res)


def _find_repl(n, repls, includes=[]):
    for i in range(len(repls) - 1, -1, -1):
        if repls[i][2][0] == n:
            return repls[i]
    for incair in includes:
        if incair.fakeName == n:
            return (
                incair.fastWalk[0],
                incair.fastWalk[1],
                incair.getRepl()
            )
    return None


def _is_operator(n):
    if n.t != 'const':
        return False
    return n.v in E.OPERATORS


def _walk_arr(arr, repls, isFinal, includes):
    resC = CONST
    resS = set()
    resE = []
    consti = []
    for i, e in enumerate(arr):
        (lC, lS, lE) = _walk(e, repls, includes)
        if lC == CONST:
            consti.append(i)
        if lC < resC:
            resC = lC
        resS.update(lS)
        resE.append(lE)
    if isFinal or resC != CONST:
        for i in consti:
            (lC, resE[i]) = _run((CONST, resE[i]), repls, includes)
            if lC < resC:
                resC = lC
    if isFinal:
        resC = NOCONST
    return (resC, resS, resE)


def _walk(el, repls, includes):
    if type(el) in [list, tuple]:
        (resC, vars, res) = _walk_arr(el, repls, True, includes)
        if type(el) == tuple:
            res = tuple(res)
        return (resC, vars, res)
    if not isinstance(el, E):
        return (NOCONST, set(), el)
    if _isConst(el):
        return (CONST, set(), el)
    if el.t != 'f':
        return (NOCONST, set(), el)
    if el.v == '`apply':
        if (
            len(el) == 2
            and el[0].t == 'const'
            and el[0].v == '⏞'
        ):
            (c, s, e) = _walk(el[1], repls, includes)
            (c, e) = _run((c, e), repls, includes)
            if c == CONST:
                return _include(e, includes)
            return (c, s, E.func('`pipely', [
                E.func('`apply', el[:1]),
                e
            ]))
        if len(el.args) > 1:
            return _walk(E.func(
                '`pipely',
                [E.func('`apply', el[:1])] + el[1:]
            ), repls, includes)
        res = _find_repl(el[0], repls, includes)
        if res is None:
            if _is_operator(el[0]):
                return (NOCONST, set(), el)
            log(WARN, 'Not found apply (consty._walk) =>', el)
            return (NOCONST, {el[0]}, el)
        if res[0] == CONST:
            return (CONST, res[1], res[2][2])
        return (res[0], res[1], el)
    elVars = el.whereTheVars()
    if elVars[0] >= 0:
        (expri, varsi, vars, ignis) = elVars
        ignis.append(varsi)
        subrepls = repls + [(NOCONST, {e}, E.func(
            '`repl',
            [e, [], None]
        )) for e in vars]
        gresC = CONST
        expC = None
        gresS = set()
        expS = None
        gresE = []
        for i, arg in enumerate(el.args):
            if i in ignis:
                gresE.append(arg)
                continue
            (resC, resS, resE) = _walk(
                arg,
                subrepls if i == expri else repls,
                includes
            )
            (resC, resE) = _run(
                (resC, resE),
                subrepls if i == expri else repls,
                includes
            )
            if i == expri:
                expC = resC
                expS = resS
            else:
                if resC < gresC:
                    gresC = resC
                gresS.update(resS)
            gresE.append(resE)
        if el.v == '`repl' and len(el[1]) == 0:
            return (expC, expS, E.func(el.v, gresE))
        for v in vars:
            expS.discard(v)
        gresS.update(expS)
        return (NOCONST, gresS, E.func(el.v, gresE))
    if el.v == ';':
        subrepls = repls[:]
        for e in el[:-1]:
            subrepls.append(_walk(e, subrepls, includes))
        res = _walk(el[-1], subrepls, includes)
        return (res[0], res[1], E.func(
            ';',
            [e[2] for e in subrepls[len(repls):]] + [res[2]]
        ))
    if el.v == '≻' and len(el) == 3:
        return (el[2][0], el[2][1], el)
    if el.v == '`pipely':
        resE = el[1:]
        resF = _walk(el[0], repls, includes)
    else:
        resE = el[:]
        resF = (NOCONST, set(), None)
    if el.v == '≻':
        resE = resE[1:]
    (resC, resS, resE) = _walk_arr(resE, repls, False, includes)
    resS.update(resF[1])
    if el.v == '`pipely':
        resE = E.func('`pipely', [resF[2]] + resE)
    else:
        resE = E.func(el.v, resE)
    if el.v == '≻':
        resE.args = [el[0]] + resE.args
    if len(resS) > 0:
        resC = NOCONST
    return (resC, resS, resE)


# TODO one thread!
class MACRO:
    data = {}

    class Uncert(Exception):
        def __str__(self):
            return '%s AS %s' % (
                repr(self.args),
                str(self.obj)
            )

    def insert(obj):
        id = 0 if len(MACRO.data) == 0 else max(MACRO.data) + 1
        MACRO.data[id] = obj
        return id

    def get(id):
        obj = MACRO.data.get(id)
        if obj is None:
            raise Exception('Wrong id given: %s' % repr(id))
        return obj

    def clear():
        MACRO.data = {}

    def uncert(id, args):
        e = MACRO.Uncert()
        e.obj = MACRO.get(id)
        e.args = args[1:]
        raise e

    def find_throwed_uncert(e):
        while True:
            if e is None:
                break
            if isinstance(e, MACRO.Uncert):
                break
            e = e.__context__
        return e

    class CTX:
        def __init__(self, tpl):
            self.deac = tpl is None
            if self.deac:
                return
            self.ex, self.data = tpl

        def __enter__(self):
            if self.deac:
                return lambda x: x
            self.d = MACRO.data
            MACRO.data = self.data
            return self.ex

        def __exit__(self, *args):
            if self.deac:
                return
            MACRO.data = self.d


def _gc(el, repls, rmSucc):
    if type(el) in [list, tuple]:
        if len(el) == 0:
            return (False, el)
        res = [_gc(e, repls, rmSucc) for e in el]
        if not max(e[0] for e in res):
            return (False, el)
        res = [e[1] for e in res]
        if type(el) == tuple:
            res = tuple(res)
        return (True, res)
    if not isinstance(el, E):
        return (False, el)
    if el.t != 'f':
        return (False, el)
    if len(el.args) == 0:
        return (False, el)
    if el.v == '`apply':
        repl = _find_repl(el[0], repls)
        if repl is None:
            if not _is_operator(el[0]):
                log(WARN, 'Not found apply (consty._gc) =>', el)
            return (False, el)
        repl[0] = True
    if el.v == ';':
        # [use? gced? repl]
        isChange = False
        subrepls = [[False, False, e] for e in el[:-1]]
        (c, resE) = _gc(el[-1], repls + subrepls, rmSucc)
        if c:
            isChange = True
        isDo = True
        while isDo:
            isDo = False
            for i in range(len(subrepls) - 1, -1, -1):
                if (
                    subrepls[i][2][0].t == 'const'
                    and subrepls[i][2][0].v == '*'
                ):
                    subrepls[i][0] = True
                if not subrepls[i][0] or subrepls[i][1]:
                    continue
                isDo = True
                (c, res) = _gc(
                    subrepls[i][2],
                    repls + subrepls[:i],
                    rmSucc
                )
                if c:
                    subrepls[i][2] = res
                    isChange = True
                subrepls[i][1] = True
        nsubrepls = [e[2] for e in subrepls if e[0]]
        if len(nsubrepls) != len(subrepls):
            isChange = True
        if not isChange:
            return (False, el)
        if len(nsubrepls) == 0:
            return (True, resE)
        return (True, E.func(
            ';',
            nsubrepls + [resE]
        ))
    if (
        E.isFunc(el, '≻')
        and len(el) == 3
        and E.isFunc(el[1], '`cases')
        and len(el[1]) == 2
        and E.isFunc(el[1][1], '≻')
    ):
        if len(el[1][1]) < 3 and rmSucc:
            return (True, _gc(
                el[1][0][0], repls, rmSucc
            )[1])
        cond = _gc(el[1][0], repls, rmSucc)[1]
        res = _gc(el[1][1], repls, rmSucc)[1]
        if E.isFunc(res, '`cases'):
            res = res.args
        else:
            res = [res]
        return (True, E.func('`cases', [cond] + res))
    if el.v == '≺':
        return (False, el)
    elVars = el.whereTheVars()
    if elVars[0] >= 0:
        (expri, varsi, vars, ignis) = elVars
        ignis.append(varsi)
        res = E.func(el.v, [])
        subrepls = repls + [
            [False, False, E.func('`repl', [v, [], None])]
            for v in vars
        ]
        isChange = False
        for i, arg in enumerate(el.args):
            if i in ignis:
                res.args.append(arg)
                continue
            (c, e) = _gc(
                arg,
                subrepls if i == expri else repls,
                rmSucc
            )
            if c:
                isChange = True
                res.args.append(e)
            else:
                res.args.append(arg)
        unusedVaris = [
            i
            for i in range(len(vars))
            if not subrepls[len(repls) + i][0]
        ]
        if len(unusedVaris) > 0:
            log(INFO, 'Unused vars', [vars[i] for i in unusedVaris], 'in:')
            log(INFO, res)
            vars = vars[:]
            for i in unusedVaris:
                # TODO that's normal?
                v = E()
                v.t = 'const'
                v.v = '_unused'
                vars[i] = E.func('msub', [v, vars[i]])
            if isinstance(res[varsi], list):
                res.args[varsi] = vars
            else:
                assert len(vars) == 1
                res.args[varsi] = vars[0]
            isChange = True
        if isChange:
            return (True, res)
        else:
            return (False, el)
    args = [_gc(e, repls, rmSucc) for e in el.args]
    if (
        el.v == '`pipely'
        and args[0][1].isFunc('`apply')
        and len(args[0][1]) == 1
    ):
        return (True, E.func(
            '`apply',
            [args[0][1][0]] + [e[1] for e in args[1:]]
        ))
    if not max(e[0] for e in args):
        return (False, el)
    return (True, E.func(
        el.v,
        [e[1] for e in args])
    )


def consty(s):
    with MACRO.CTX(seriessy) as ex:
        s = ex(s)
    includes = Incair.createIncludes()
    (c, _, s) = _walk(s, [], includes)
    (c, s) = _run((c, s), [], includes)
    if len(includes) > 1:
        s = Incair.withIncludes(includes, s)
    (c, s) = _gc(s, [], True)
    if c:
        log(INFO, 'GCed')
    else:
        log(INFO, 'no GCed')
    return s


def load(s):
    e = E()
    e.t = 'string'
    e.v = s
    includes = Incair.createIncludes()
    (c, _, s) = _include(e, includes)
    (c, s) = _run((c, s), [], includes)
    if len(includes) > 1:
        s = Incair.withIncludes(includes, s)
    (c, s) = _gc(s, [], True)
    s = Unclosure.bind(s)
    MACRO.clear()
    s = cody(s, 'py', 'generators/py.py')
    s = execute(s)
    s = s()
    d = MACRO.data
    MACRO.clear()
    return (s, d)


seriessy = load('operators/series')
