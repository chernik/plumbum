import sys
import os
import time

from tests import test_all
import filer
import jsserver
import logs


HELP = """
--------------- HELP ------------------
Args for usage (more info in README.md):

test coverage
test speed
test
filer <fold>
"""


def main():
    args = sys.argv[1:]
    if args == ['test', 'coverage']:
        logs.level = logs.WARN
        os.system("coverage run --omit 'penv/*,jsserver/*,main.py' -m main test")
        os.system("coverage html")
        print("Open htmlcov/index.html for check coverage")
        return
    if args == ['test', 'speed']:
        logs.level = logs.ERROR
        m = 100
        for i in range(50):
            t = time.time()
            test_all()
            t = time.time() - t
            if t < m:
                m = t
        print('Test time =>', m)
        return
    if args == ['test']:
        logs.level = logs.WARN
        test_all()
        return
    if args == ['jsserver']:
        logs.level = logs.WARN
        jsserver.run()
        return
    if len(args) == 2 and args[1] == 'filer':
        logs.level = logs.WARN
        filer.walk(sys.argv[2])
        return
    if args == []:
        print(HELP)
        return
    print('-------- ERROR --------')
    print('Wrong args, run without args for help')


main()
