INFO = 0
WARN = 1
ERROR = 2

level = INFO


def log(lvl, *args):
    if(lvl >= level):
        print(*args)
