from tree import E


class TE:  # Нода графа
    class W:  # Что вообще это за нода
        NONE = 0  # Неизвестно
        CONST = 1  # Константа (число, строка и прочее)
        VAR = 2  # Переменная или имя функции
        TYPED = 3  # Конкретный тип
        FUNC = 4  # Функция
        RET = 5  # Результат функции
        ANON = 6  # Анонимная нода
        REF = 7  # Ссылка на другую ноду

    def __getW(self):
        if self.w == TE.W.NONE:
            return 'NONE'
        elif self.w == TE.W.CONST:
            return 'CONST'
        elif self.w == TE.W.VAR:
            return 'VAR'
        elif self.w == TE.W.TYPED:
            return 'TYPED'
        elif self.w == TE.W.FUNC:
            return 'FUNC'
        elif self.w == TE.W.RET:
            return 'RET'
        elif self.w == TE.W.ANON:
            return 'ANON'
        elif self.w == TE.W.REF:
            return 'REF'

    def __repr__(self):
        if self.w == TE.W.CONST:
            s = f' from {repr(self.el)}'
        elif self.w == TE.W.VAR:
            s = f' aka {repr(self.el)}'
        elif self.w == TE.W.TYPED:
            s = f' means {repr(self.t)}'
        else:
            s = ''
        s = f'{self.__getW()} <t{self.id}>{s}'
        if len(self.deps) != 0:
            if self.w == TE.W.FUNC:
                s = f"""{s}: <t{self.deps[0]}>({
                    ", ".join(f"<t{e}>" for e in self.deps[1:])
                })"""
            elif self.w == TE.W.RET:
                s = f'{s}: R(<t{self.deps[0]}>)'
            else:
                s = f"""{s}: {
                    " | ".join(f"<t{e}>" for e in self.deps)
                }"""
        return s

    def __str__(self):
        return f'{repr(self)}\n{str(self.el)}'


class TEgraph:  # Граф из нод
    def __init__(self):
        self.tes = []  # Все ноды
        self.vars = []  # Все переменные
        self.fake = None

    def make(self):
        te = TE()  # Создаем ноду
        self.tes.append(te)  # Сохраняем
        te.id = len(self.tes) - 1  # Берем id как индекс в массиве
        te.w = TE.W.NONE  # Пока неясно зачем создали
        te.el = None  # Пока нет ассоциации с выражением
        te.deps = []  # Зависимости в графе
        te.t = None  # Определенный тип
        return te

    def makeFake(self):  # Создаем фейковую ноду
        te = self.make()  # Создаем обычную
        self.tes = self.tes[:-1]  # Убираем из списка
        te.id = -2  # Зарезервированный id для фейка
        self.fake = te  # Сохраняем в другом месте
        return te

    def makeUnknown(self, el):
        te = self.make()  # Нода с неизвестным выражением
        te.el = el
        return te.id

    def makeConst(self, c):
        te = self.make()  # Создаем ноду для константы
        te.w = TE.W.CONST
        te.el = c
        return te.id

    def makeVar(self, n):
        te = self.make()  # Создаем ноду для переменной
        te.w = TE.W.VAR
        te.el = n  # Имя переменной
        return te.id

    def at(self, tei):
        if tei == -2:  # Захотели фейк
            return self.fake
        assert tei >= 0  # Индекс должен быть нормальным
        return self.tes[tei]  # Нода по id

    def linkSimple(self, si, di):  # d зависит от s
        self.at(di).deps.append(si)
        return di

    def linkFunc(self, fi, ri, ais):  # fi зависит от ri(ais*)
        tef = self.make()
        tef.w = TE.W.FUNC
        tef.deps = [ri] + ais  # Зависимости ноды для функции
        return self.linkSimple(tef.id, fi)

    def makeAnon(self, el):  # Создаем анонимную ноду
        tef = self.make()
        tef.w = TE.W.ANON
        tef.el = el
        return tef.id

    def makeCall(self, fi, ais, el):  # Зависимость для вызова
        # Зависимость от поданных типов
        self.linkFunc(fi, self.makeAnon(el), ais)
        ter = self.make()  # Тип возврата
        ter.el = el
        ter.w = TE.W.RET
        ter.deps.append(fi)
        return ter.id

    def makeType(self, t):  # Нода с типом
        te = self.make()
        te.w = TE.W.TYPED
        te.t = t
        return te.id

    def makeRef(self, tei):  # Ссылка на другую ноду
        te = self.make()
        te.w = TE.W.REF
        te.deps.append(tei)
        return te.id

    def _detectLoops(self, tei, walked, loops):
        # Детектим циклы
        if tei in walked:  # Уже проходили ноду
            if tei not in loops:  # Сохраняем цикличную ноду
                loops.append(tei)
            teri = self.makeRef(tei)  # Подменяем на ссылку
            walked.append(teri)  # Говорим что прошли ссылку
            return teri
        walked.append(tei)
        for i, teai in enumerate(self.at(tei).deps):
            res = self._detectLoops(teai, walked, loops)
            if res < 0:
                continue
            self.at(tei).deps[i] = res
        return -1

    def _detectTrees(self, tei, trees):
        # Детектим деревья
        te = self.at(tei)
        if te.w == TE.W.REF:
            return False  # Имеем цикл, значит, не дерево
        isTree = True  # Дефолтно мы в дереве
        subtrees = []  # Поддеревья
        for teai in te.deps:  # Обходим потомков
            if self._detectTrees(teai, trees):  # Дерево?
                subtrees.append(teai)
            else:  # Не дерево
                isTree = False
        if isTree:  # Все потомки -- деревья либо лист
            return True
        # Иначе сохраняем все деревья-потомки
        [trees.append(teai) for teai in subtrees]
        return False

    def splitCircles(self, tei):  # Начинаем обход с ноды tei
        walked = []  # Уже обойденные ноды
        loops = []  # Узлы-рекурсии
        te = self.makeFake()  # Фейковая нода для обхода
        te.deps.append(tei)
        res = self._detectLoops(te.id, walked, loops)
        assert res == -1  # Это не должен быть цикл
        assert te.id in walked  # Фейковая нода должна быть пройдена
        # Смотрим, какие ноды не обошли
        ignored = [te.id for te in self.tes if te.id not in walked]
        trees = []  # Вершины графов без циклов
        res = self._detectTrees(te.id, trees)
        if res:  # Если все есть дерево, то оно одно
            assert trees == [te.id]
            trees = [tei]  # Подменяем фейковую в таком случае
        # Убираем циклы из деревьев (актуально для листьев)
        # Покрываем все цикличные ноды рефами и удаляем из trees
        for teai in loops:
            if teai in trees:  # Удаляем из trees
                trees.remove(teai)
            for tea in self.tes:
                if tea.w == TE.W.REF:  # Сами рефы не проверяем
                    continue
                if teai in tea.deps:  # Нашли
                    teaidi = tea.deps.index(teai)  # Подменяем
                    tea.deps[teaidi] = self.makeRef(teai)
        # Проверяем, что мы не взяли фейковую ноду
        assert te.id not in loops
        assert te.id not in ignored
        assert te.id not in trees
        self.fake = None  # Убираем фейковую ноду
        return (loops, trees, ignored)

    def simplify(self, tei):  # Упрощаем дерево
        te = self.at(tei)
        if len(te.deps) == 0:  # Листы все CONST
            return [TE.W.CONST, True, tei]
        if te.w == TE.W.REF:  # Ссылка
            return [te.w, False, te.deps[0]]
        if te.w in [TE.W.FUNC, TE.W.RET]:  # Необходимые ноды
            return [te.w, False] + [self.simplify(teai) for teai in te.deps]
        # Остальное сжимаем как ANON
        res = []
        for teai in te.deps:
            resi = self.simplify(teai)
            if resi[0] == TE.W.ANON:
                res += resi[2:]
            else:
                res.append(resi)
        if len(res) == 1:
            return res[0]
        else:
            return [TE.W.ANON, False] + res

    def toEnode(_, n, args):  # Создаем ноду-функцию
        if n in [',', ':', '*']:  # Нативные функции
            return E.func(n, args)
        en = E()  # Наши функции
        en.t = 'const'
        en.v = n
        return E.func('`apply', [en] + args)

    def toEref(self, tei):  # L(tei) -- переменная
        return self.toEnode('L', [tei])

    def toE(self, arr):  # Перевод упрощенной формы в дерево
        if arr[1]:
            te = self.at(arr[2])
            if te.w == TE.W.CONST:
                # C(10) -- тип из константы
                return self.toEnode('C', [te.el])
            if te.w == TE.W.NONE:
                # E(<необработанное поддерево>)
                return self.toEnode('E', [te.el])
            if te.t is None:
                # T() -- произвольный тип
                return self.toEnode('T', [])
            # T(type) -- конкретный тип
            return self.toEnode('T', [te.t])
        if arr[0] == TE.W.FUNC:
            # F(tr, A(t1, t2, ...)) -- функция с аргументами типов t1, t2, ...
            # и возвращаемым типом tr
            return self.toEnode('F', [
                self.toE(arr[2]),
                self.toEnode('A', [self.toE(tei) for tei in arr[3:]])
            ])
        if arr[0] == TE.W.RET:
            # R(t) -- тип, возвращенный функцией типа t
            return self.toEnode('R', [self.toE(arr[2])])
        if arr[0] == TE.W.REF:
            # Переменная
            return self.toEref(arr[2])
        # t1 * t2 * ... -- выбор типов
        return self.toEnode('*', [self.toE(tei) for tei in arr[2:]])

    def toEsystem(self, loops):  # Система уравнений
        # t1: t2, t3: t4, ...
        return self.toEnode(',', [
            self.toEnode(':', [self.toEref(tei), self.toE(self.simplify(tei))])
            for tei in loops
        ]).squeeze()

    def toType(self, tei):
        te = self.at(tei)
        if te.w == TE.W.CONST:
            return repr(te.el)
        if te.w == TE.W.NONE:
            return 'E'
        if te.t is None:
            return 'T'
        return te.t

    def toStr(self, arr):
        if arr[1]:
            return self.toType(arr[2])
        if arr[0] == TE.W.FUNC:
            return f"""{self.toStr(arr[2])}({
                ", ".join(self.toStr(teai) for teai in arr[3:])
            })"""
        if arr[0] == TE.W.RET:
            return f'R_{self.toStr(arr[2])}'
        if arr[0] == TE.W.REF:
            return f'Lt{arr[2]}'
        return f"""[{
            " | ".join(self.toStr(arri) for arri in arr[2:])
        }]"""


class Types:
    def _addVar(n, tes, vars):
        tei = tes.makeVar(n)
        vars.append((n, tei))  # Сохраняем в стек
        return tei

    def _getVar(n, vars):
        for i in range(len(vars)):
            if vars[-i-1][0] == n:  # Одно и то же имя
                return vars[-i-1][1]
        raise Exception(f'Not found var {n}')

    def _walkSeries(el, tes, vars):
        tefi = tes.makeAnon(el)  # Нода для series
        if el[0] == 'recN':  # Рекурсия
            # Обходим зарезервированные значения
            for args, res in el[1]:
                # Аргументы
                teais = [Types._walk(e, tes, vars) for e in args]
                # Результат
                teri = Types._walk(res, tes, vars)
                tes.linkFunc(tefi, teri, teais)
            # Добавляем переменные
            subvars = vars[:]
            teais = [Types._addVar(e, tes, subvars) for e in el[2]]
            # Результат
            teri = Types._walk(el[3], tes, subvars)
            return tes.linkFunc(tefi, teri, teais)
        if el[0] == 'arrN':  # Массив
            # Типы элементов
            teris = [Types._walk(e, tes, vars) for e in el[3]]
            teri = tes.makeAnon(el)  # Тип возвращенного значения
            # Он зависит от всех элементов массива
            [tes.linkSimple(tei, teri) for tei in teris]
            # Линкуем функцию
            return tes.linkFunc(tefi, teri, [tes.makeType('index')])
        # Неизвестный series
        tes.at(tefi).w = TE.W.NONE
        return tefi

    def _walk(el, tes, vars):
        if type(el) != E:
            return tes.makeUnknown(el)
        if el.t == 'number':  # Константа
            return tes.makeConst(el)
        if el.t != 'f':
            return tes.makeUnknown(el)
        if el.v == ';':
            subvars = vars[:]
            for e in el[:-1]:
                Types._walk(e, tes, subvars)
            return Types._walk(el[-1], tes, subvars)
        if el.v == '`repl':  # Новая переменная
            tefi = Types._addVar(el[0], tes, vars)  # Тип задаваемой переменной
            # Типы параметров функции
            teais = [Types._addVar(e, tes, vars) for e in el[1]]
            teri = Types._walk(el[2], tes, vars)  # Тип возвращаемого значения
            if len(teais) == 0:  # Это не функция
                return tes.linkSimple(teri, tefi)
            else:  # Это функция
                return tes.linkFunc(tefi, teri, teais)
        if el.v == '`apply':  # Применение переменной
            tefi = Types._getVar(el[0], vars)  # Имя ноды для переменной
            # Типы параметров функции
            teais = [Types._walk(e, tes, vars) for e in el[1:]]
            if len(teais) == 0:  # Просто значение
                return tefi
            else:  # Результат функции
                return tes.makeCall(tefi, teais, el)
        if el.v == '`series':
            return Types._walkSeries(el, tes, vars)
        # Что-то неизвестное
        return tes.makeUnknown(el)

    def walk(el):
        tes = TEgraph()
        teiRet = Types._walk(el, tes, [])
        loops, trees, ignored = tes.splitCircles(teiRet)
        sel = tes.toEsystem(loops)
        print('=== TYPES WALK RESULT ===')
        print('IGNORED >>>')
        [print(tes.at(tei)) for tei in ignored]
        print('TREES >>>')
        [print(
            f'{repr(tes.at(teai)):>50} >>> '
            f'{tes.toStr(tes.simplify(teai))}'
        ) for teai in trees]
        print('LOOPS >>>')
        [print(
            f'{repr(tes.at(teai)):>50} >>> '
            f'{tes.toStr(tes.simplify(teai))}'
        ) for teai in loops]
        print('SYSTEM >>>')
        print(sel)
