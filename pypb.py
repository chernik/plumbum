import ast
from tree import E


BINOPS = {
    ast.Add: '+',
    ast.Eq: '≡'
}
"""
class ast.Add
class ast.Sub
class ast.Mult
class ast.Div
class ast.FloorDiv
class ast.Mod
class ast.Pow
class ast.LShift
class ast.RShift
class ast.BitOr
class ast.BitXor
class ast.BitAnd
class ast.MatMult

class ast.Eq
class ast.NotEq
class ast.Lt
class ast.LtE
class ast.Gt
class ast.GtE
class ast.Is
class ast.IsNot
class ast.In
class ast.NotIn
"""


def _toStr(s):
    if isinstance(s, ast.AST):
        ss = ast.dump(s)
    else:
        ss = str(s)
    return f"<<< UNKNOWN {ss}: {type(s)} >>>"


def _parse(s):
    return ast.parse(s).body


def _binop(s):
    return BINOPS.get(type(s))


def _unknown(s):
    return E.err(_toStr(s))


def _var(s):
    e = E()
    e.t = 'const'
    if s == '_S':
        e.v = '*'
    else:
        e.v = s
    return e


def _const(s):
    e = E()
    if type(s.value) in [int, float]:
        e.t = 'number'
    elif isinstance(s.value, str):
        e.t = 'stirng'
    else:
        return _unknown(s)
    e.v = str(s.value)
    return e


def _arr(s):
    if len(s) == 1:
        return s[0]
    else:
        return E.func(';', s)


def _walk(s):
    if isinstance(s, list):
        return [_walk(e) for e in s]
    if isinstance(s, ast.arguments):
        return _walk(s.args)
    if isinstance(s, ast.arg):
        return _var(s.arg)
    if isinstance(s, ast.Expr):
        return _walk(s.value)
    if isinstance(s, ast.Constant):
        return _const(s)
    if isinstance(s, ast.Name):
        name = _var(s.id)
        if isinstance(s.ctx, ast.Store):
            return name
        if isinstance(s.ctx, ast.Load):
            return E.func('`apply', [name])
        return _unknown(s)
    if isinstance(s, ast.FunctionDef):
        name = _var(s.name)
        args = _walk(s.args)
        body = _walk(s.body)
        return E.func('`repl', [
            name,
            args,
            _arr(body)
        ])
    if isinstance(s, ast.BinOp):
        name = _binop(s.op)
        if name is None:
            return _unknown(s)
        args = [_walk(s.left), _walk(s.right)]
        return E.func(name, args)
    if isinstance(s, ast.Assign):
        if len(s.targets) != 1:
            return _unknown(s)
        name = _walk(s.targets[0])
        body = _walk(s.value)
        return E.func('`repl', [
            name,
            [],
            body
        ])
    if isinstance(s, ast.Call):
        func = _walk(s.func)
        args = _walk(s.args)
        return E.func('`pipely', [func] + args)
    if isinstance(s, ast.Assert):
        return E.func('`apply', [_var('⊧'), _walk(s.test)])
    if isinstance(s, ast.Compare):
        if len(s.ops) != 1:
            return _unknown(s)
        name = _binop(s.ops[0])
        if name is None:
            return _unknown(s)
        args = [_walk(s.left), _walk(s.comparators[0])]
        return E.func(name, args)
    return _unknown(s)


def convert(s):
    s = _parse(s)
    s = _walk(s)
    s = _arr(s)
    s = s.squeeze()
    return s
