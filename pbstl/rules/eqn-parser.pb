\\
r = \overbrace{\text{replacer}}; \\
f(a) = ( \\

    e_0(i) = r( \\
        i, \\
        \prec(
            ?(0) \to ?(1) \to ?(2)
        ).(2), \\
        \prec(
            r(?(0), ?(1), ?(2))
        ).(2), \\
        \otimes); \\

    e_1(i) = r( \\
        i, \\
        \prec(
            ?(0) \to ?(1)
        ).(2), \\
        \prec(
            r(?(0), d, ?(1))
        ).(2), \\
        \otimes); \\

    e_2(i) = r( \\
        i, \\
        \prec(
            m(?(0), ?(...1))
        ).(2), \\
        \prec(
            m(?(0))
        ).(2), \\
        \otimes); \\

    e_3(i) = r( \\
        i, \\
        \prec(
            r(?(0), ?(1), ?(2))
        ).(2), \\
        \prec(
            a_s = \prec(?(0)).(2);
            a_f(d) = ?(1);
            a_d = \prec(?(2)).(2);
            a_q(d) = a_f(d(\text{?}, a_d));
            a_r(a) = \left\{ r(a, a_s, a_q) \right\};
            a_r
        ).(2), \\
        \otimes); \\

    e_4(i) = r( \\
        i, \\
        \prec(
            m(?(0))
        ).(2), \\
        \prec(
            a_s = \prec(?(0)).(2);
            a_r(a) = m(a, a_s);
            a_r
        ).(2), \\
        \otimes); \\

    a_0(i) = a(i + 2); \\

    a_1 = \left\{
        e_4(e_3(e_2(e_1(e_0(i)))))
        \mid i \in a_0
    \right\}; \\

    e_5(d) = d(1, a_1).(\text{?}, \prec(
        a_r(r, m, q, l, n) = \left\{ ?(...1) \right\};
        a_r
    ).(2)); \\

    a_2 = r(
        \prec(0).(2),
        \prec(?(0)).(2),
        e_5,
        \otimes
    ); \\

    \succ(a_2) \\

); \\
f
