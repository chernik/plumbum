f(x) = {
    g(y,z) = {
        h(w) = x + y + z + w;
        h
    };
    g
};
r = f(2).(3,4).(5);
\models(r \equiv 14)
