r = \overbrace{\text{operators/series}}; \\

s = \prec(
    \left\{ i \in a \mid i > 3 \right\},
    \left\{ i + 3 \mid i \in a \right\},
    \left\{ a+b+с \mid a \in A, b \in B, c \in C \right\},
    \left\{ a \in A, b \in B \mid a + b > 3 \right\},
    \left\{ i \mid i \right\},
    \left\{ i \mid i + 1 \in A \right\},
    \left\{ ...(i + j - 2) \mid i \in A, j \in A \right\},
    \left\{ \left{ i \in a + b + c \mid i < 0 \right\} \mid a \in A, b \in B, c \in C \right\}
); \\

\left\{ r, s \right\}
