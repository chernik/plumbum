# Plumbum

Functional DSL over TeX (over formula description part of it) which allows to translate those formulas directly into code snippets.

Currently supported target languages:

* Javascript
* Python 3

More detailed description is avaiable in file [description.pdf](/readme_stuff/description.pdf) (only in Russian currently)

## Requirements

*Python 3* is required to use that app. Additional requirements are listed in [requirements.txt](/requirements.txt) and could be installed through pip automatically. Usage of virtual environment is recommended in case of library version conflict.
```bash
python -m pip install -r requirements.txt
```

## Usage

```bash
python main.py <mode> [args...]
```

Currently there is one mode supported: filer. It will translate all files with .pb extension into files with files with code. Target language is automatically chosen based on file name.

* *example.js.pb* will produce Javascript code in *example.js* 
* *example.py.pb* will produce Python 3 code in *example.py*

## Testing

`python3 main.py test` for simple check

`python3 main.py test coverage` for lookup coverage

`python3 main.py test speed` for check speed (about 3.5 min waiting needed)

## Speed & coverage

| Label | Speed | Coverage |
| --- | --- | --- |
| Init | 2.7436625957489014 sec | Not presented |
| Seriessy added | 4.239192962646484 sec | 85% |
| Optimize imports | 4.103218078613281 sec | 85% |
| Consty added for all tests (ignoring analyze tests) | 5.583238840103149 sec | 87% |
| Add logger | 4.86099910736084 sec | 88% |
| Clean mapN & genN, add flatmapN | 5.214419364929199 | 89% |
